﻿namespace ChatServer.Contract.Query.Outgoing
{
    public class QueryNumberOfMessagesSentResponse
    {
        public int NumberOfMessages { get; private set; }

        public QueryNumberOfMessagesSentResponse(int numberOfMessages)
        {
            NumberOfMessages = numberOfMessages;
        }
    }
}

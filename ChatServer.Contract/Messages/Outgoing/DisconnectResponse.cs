﻿namespace ChatServer.Contract.Messages.Outgoing
{
    public class DisconnectResponse
    {
        public string Message { get; private set; }

        public DisconnectResponse(string message)
        {
            Message = message;
        }
    }
}

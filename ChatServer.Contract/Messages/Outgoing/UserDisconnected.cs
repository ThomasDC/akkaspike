﻿namespace ChatServer.Contract.Messages.Outgoing
{
    public class UserDisconnected
    {
        public UserDisconnected(string userName)
        {
            UserName = userName;
        }

        public string UserName { get; private set; }
    }
}

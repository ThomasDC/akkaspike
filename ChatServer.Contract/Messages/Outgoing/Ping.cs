﻿namespace ChatServer.Contract.Messages.Outgoing
{
    public class Ping
    {
        public int SequenceNumber { get; private set; }

        public Ping(int sequenceNumber)
        {
            SequenceNumber = sequenceNumber;
        }
    }
}

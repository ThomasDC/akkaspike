﻿namespace ChatServer.Contract.Messages.Outgoing
{
    public class UserConnected
    {
        public string UserName { get; private set; }

        public UserConnected(string userName)
        {
            UserName = userName;
        }
    }
}

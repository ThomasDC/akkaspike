﻿namespace ChatServer.Contract.Messages.Outgoing
{
    public class MessageBroadcast
    {
        public string Message { get; private set; }

        public string BroadcastedByUserName { get; private set; }

        public MessageBroadcast(string message, string broadcastedByUserName)
        {
            Message = message;
            BroadcastedByUserName = broadcastedByUserName;
        }
    }
}

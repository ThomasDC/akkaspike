﻿namespace ChatServer.Contract.Messages.Incoming
{
    public class EventBusMessage
    {
        public string Message { get; }

        public EventBusMessage(string message)
        {
            Message = message;
        }
    }
}

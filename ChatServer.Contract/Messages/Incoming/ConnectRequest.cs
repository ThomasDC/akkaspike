﻿namespace ChatServer.Contract.Messages.Incoming
{
    public class ConnectRequest
    {
        public string UserName { get; private set; }

        public ConnectRequest(string userName)
        {
            UserName = userName;
        }
    }
}

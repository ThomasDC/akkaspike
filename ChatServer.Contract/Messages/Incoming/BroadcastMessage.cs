﻿namespace ChatServer.Contract.Messages.Incoming
{
    public class BroadcastMessage
    {
        public string Message { get; private set; }

        public BroadcastMessage(string message)
        {
            Message = message;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Akka.Actor;
using Akka.Util.Internal;
using ChatServer.Contract.Messages.Incoming;
using ChatServer.Contract.Messages.Outgoing;
using ChatServer.Contract.Query.Incoming;
using ChatServer.Contract.Query.Outgoing;

namespace ChatServer
{
    internal class ChatServerActor : TypedActor, // TODO: convert to ReceiveActor! (TypedActor is obsolete since it relies on reflection...)
        IHandle<ConnectRequest>, 
        IHandle<BroadcastMessage>, 
        IHandle<DisconnectRequest>,
        IHandle<SendPing>,
        IHandle<Terminated>,
        IHandle<EventBusMessage>,
        // Query
        IHandle<QueryNumberOfMessagesSentRequest>,
        IHandle<QueryNumberOfMessagesSentResponse>,
        IHandle<QueryNumberOfConnectedClientsRequest>
    {
        private readonly IDictionary<IActorRef, string> _chatClients = new Dictionary<IActorRef, string>();
        private int _numberOfMessagesSent;

        private int _pingSequenceNumber;

        public ChatServerActor()
        {
            Context.System.Scheduler.ScheduleTellRepeatedly(TimeSpan.FromSeconds(5), TimeSpan.FromSeconds(1), Self, new SendPing(), Self);
        }

        public void Handle(ConnectRequest message)
        {
            Console.WriteLine($"Connecting {message.UserName}");

            TellClients(new UserConnected(message.UserName));

            _chatClients.Put(Sender, message.UserName);

            Context.Watch(Sender);

            Sender.Tell(new ConnectResponse($"Welcome, {message.UserName}!"), Self);
        }

        public void Handle(BroadcastMessage message)
        {
            var broadcastedBy = _chatClients[Sender];

            Console.WriteLine($"{broadcastedBy} broadcasted the following message: {message.Message}");

            _numberOfMessagesSent++;
            Console.WriteLine($"Number of messages sent: {_numberOfMessagesSent}");

            TellClients(new MessageBroadcast(message.Message, broadcastedBy), Sender);
        }

        public void Handle(DisconnectRequest message)
        {
            var disconnectedUser = _chatClients[Sender];

            Console.WriteLine($"{disconnectedUser} disconnected");

            _chatClients.Remove(Sender);

            Sender.Tell(new DisconnectResponse($"Sad to see you leave, {disconnectedUser}"), Self);

            TellClients(new UserDisconnected(disconnectedUser));
        }

        private void TellClients(object message, IActorRef except = null)
        {
            foreach (var client in _chatClients)
            {
                if (except == null || client.Key.Path != except.Path)
                {
                    client.Key.Tell(message, Self);
                }
            }
        }

        public void Handle(SendPing message)
        {
            Console.WriteLine($"\tSending ping {_pingSequenceNumber}");
            TellClients(new Ping(_pingSequenceNumber++));
        }

        public void Handle(Terminated message)
        {
            var disconnectedClient = _chatClients.Single(_ => _.Key.Path.Uid == message.ActorRef.Path.Uid);
            Console.WriteLine($"{disconnectedClient.Value} disconnected :(");
            _chatClients.Remove(disconnectedClient);
            TellClients(new UserDisconnected(disconnectedClient.Value));
        }

        public void Handle(QueryNumberOfMessagesSentRequest message)
        {
            var sender = Sender;
            var task = Task<int>.Factory.StartNew(() =>
            {
                // Simulate calling an async API...
                Console.WriteLine("Sleeping...");
                Thread.Sleep(5000);
                Console.WriteLine("Woke up!");
                var random = new Random();
                return random.Next(int.MaxValue);
            });

            task.ContinueWith(_ =>
            {
                // Optional transformation of the message (is treated as a regular message in the mailbox!)
                var randomNumber = _.Result;
                Console.WriteLine($"The randomly generated number was {randomNumber}");
                return new QueryNumberOfMessagesSentResponse(_numberOfMessagesSent);
            }, TaskContinuationOptions.AttachedToParent & TaskContinuationOptions.ExecuteSynchronously) // https://github.com/petabridge/akkadotnet-code-samples/blob/master/PipeTo/src/PipeTo.App/Actors/HttpDownloaderActor.cs#L98
            // https://github.com/petabridge/akkadotnet-code-samples/tree/master/PipeTo
            .PipeTo(Self, sender);
        }

        public void Handle(QueryNumberOfMessagesSentResponse message)
        {
            Console.WriteLine("QueryNumberOfMessagesSentResponse piped to the ChatServerActor");
            Sender.Tell(message);
        }

        public void Handle(QueryNumberOfConnectedClientsRequest message) // Called through Ask
        {
            try
            {
                var result = _chatClients.Count;
                Console.WriteLine($"Sender = {Sender}");
                Sender.Tell(result, Self);
            }
            catch (Exception e)
            {
                // http://getakka.net/docs/working-with-actors/sending-messages#ask-send-and-receive-future
                Sender.Tell(new Failure { Exception = e }, Self);
            }
        }

        public void Handle(EventBusMessage message)
        {
            Console.WriteLine($"Got an EventBusMessage: {message.Message}");
        }
    }
}

﻿using System;
using System.Threading.Tasks;
using Akka.Actor;
using ChatServer.Contract.Messages.Incoming;

namespace ChatServer
{
    internal class Program
    {
        static void Main(string[] args)
        {
            using (var system = ActorSystem.Create("LocalServer"))
            {
                var serverActor = system.ActorOf<ChatServerActor>("ChatServer");
                system.EventStream.Subscribe(serverActor, typeof(EventBusMessage));

                var anotherEventBusConsumer = system.ActorOf<AnotherEventBusConsumer>("AnotherEventBusConsumer");
                system.EventStream.Subscribe(anotherEventBusConsumer, typeof(EventBusMessage));

                Task.Delay(5000).ContinueWith(_ =>
                {
                    system.EventStream.Publish(new EventBusMessage("Hello from the event bus"));
                });

                Console.ReadLine();
            }
        }

        class AnotherEventBusConsumer : ReceiveActor
        {
            public AnotherEventBusConsumer()
            {
                Receive<EventBusMessage>(_ =>
                {
                    Console.WriteLine($"AnotherEventBusConsumer got an EventBusMessage: {_.Message}");
                });
            }
        }
    }
}

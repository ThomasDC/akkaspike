﻿using System;
using Akka.Actor;
using ChatClient.Contract.Messages.Incoming;
using ChatServer.Contract.Messages.Incoming;
using ChatServer.Contract.Messages.Outgoing;
using ChatServer.Contract.Query.Incoming;
using ChatServer.Contract.Query.Outgoing;

namespace ChatClient
{
    internal class ChatClientActor : TypedActor, // TODO: convert to ReceiveActor! (TypedActor is obsolete since it relies on reflection...)
        // Plumbing
        IHandle<ActorIdentity>,
        // Incoming client
        IHandle<Connect>,
        IHandle<SendMessage>,
        IHandle<Disconnect>,
        IHandle<QueryNumberOfMessages>,
        // Incoming server
        IHandle<ConnectResponse>,
        IHandle<MessageBroadcast>,
        IHandle<DisconnectResponse>,
        IHandle<UserConnected>,
        IHandle<UserDisconnected>,
        IHandle<Ping>,
        IHandle<QueryNumberOfMessagesSentResponse>
    {
        private bool _connected;
        private IActorRef _chatServer;

        public ChatClientActor(ICanTell chatServerSelection)
        {
            _connected = false;

            // Prefer IActorRef over ActorSelection -> identify underlying IActorRef through built-in Identify message (http://getakka.net/docs/working-with-actors/identifying-actors-via-actor-selection)
            chatServerSelection.Tell(new Identify(Guid.NewGuid()), Self);
        }

        public void Handle(ActorIdentity message)
        {
            _chatServer = Sender;
        }

        public void Handle(Connect message)
        {
            if (_connected)
            {
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine("Can't connect since we're already connected");
                Console.ResetColor();
            }
            else
            {
                Console.WriteLine("Connecting...");
                _chatServer.Tell(new ConnectRequest(message.UserName), Self);
            }
        }

        public void Handle(SendMessage message)
        {
            if (_connected)
            {
                Console.WriteLine($"[You]: {message.Message}");
                _chatServer.Tell(new BroadcastMessage(message.Message), Self);
            }
            else
            {
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine("*** You need to connect before being able to send messages ***");
                Console.ResetColor();
            }
        }

        public void Handle(Disconnect message)
        {
            if (_connected)
            {
                Console.WriteLine("*** Disconnecting... ***");
                _chatServer.Tell(new DisconnectRequest(), Self);
            }
            else
            {
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine("*** Already disconnected ***");
                Console.ResetColor();
            }
        }

        public void Handle(ConnectResponse message)
        {
            _connected = true;
            Console.WriteLine("*** Successfully connected to the chat ***");
        }

        public void Handle(UserConnected message)
        {
            Console.WriteLine($"*** {message.UserName} joined the chat ***");
        }

        public void Handle(MessageBroadcast message)
        {
            Console.WriteLine($"[{message.BroadcastedByUserName}] {message.Message}");
        }

        public void Handle(DisconnectResponse message)
        {
            _connected = false;
            Console.WriteLine("*** Disconnected successfully from the chat ***");
        }
        
        public void Handle(UserDisconnected message)
        {
            Console.WriteLine($"*** {message.UserName} left the chat ***");
        }

        public static Props Props(ICanTell chatServerSelection)
        {
            return Akka.Actor.Props.Create(() => new ChatClientActor(chatServerSelection));
        }

        public void Handle(Ping message)
        {
            Console.WriteLine($"\tReceived ping {message.SequenceNumber}");
        }

        public void Handle(QueryNumberOfMessages message)
        {
            Console.WriteLine("Querying the total number of messages");
            _chatServer.Tell(new QueryNumberOfMessagesSentRequest());
        }

        public void Handle(QueryNumberOfMessagesSentResponse message)
        {
            Console.WriteLine($"Received the total number of messages: {message.NumberOfMessages}");
        }
    }
}

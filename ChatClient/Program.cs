﻿using System;
using Akka.Actor;
using ChatClient.Contract.Messages.Incoming;
using ChatServer.Contract.Query.Incoming;

namespace ChatClient
{
    internal class Program
    {
        static void Main(string[] args)
        {
            using (var system = ActorSystem.Create("ChatClientActor"))
            {
                var chatServerSelection = system.ActorSelection("akka.tcp://LocalServer@127.0.0.1:8081/user/ChatServer");
                var chatClient = system.ActorOf(ChatClientActor.Props(chatServerSelection));

                while (true)
                {
                    var input = Console.ReadLine();
                    input = input ?? string.Empty;
                    if (input.StartsWith("/connect"))
                    {
                        var userName = input.Split(' ')[1];
                        chatClient.Tell(new Connect(userName));
                    }
                    else if (input == "/disconnect")
                    {
                        chatClient.Tell(new Disconnect());
                    }
                    else if (input == "/querymessages")
                    {
                        chatClient.Tell(new QueryNumberOfMessages());
                    }
                    else if (input == "/queryclients")
                    {
                        // https://stackoverflow.com/questions/32424776/akka-proper-pattern-for-nested-asks-net/32433259#32433259
                        var task = chatServerSelection.Ask<int>(new QueryNumberOfConnectedClientsRequest(), TimeSpan.FromSeconds(5));
                        task.ContinueWith(task1 =>
                        {
                            Console.WriteLine(
                                $"Received the total number of connected clients from the server: {task1.Result}");
                        });
                    }
                    else
                    {
                        chatClient.Tell(new SendMessage(input));
                    }
                }
            }
        }
    }
}

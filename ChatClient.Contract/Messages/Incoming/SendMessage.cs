﻿namespace ChatClient.Contract.Messages.Incoming
{
    public class SendMessage
    {
        public string Message { get; private set; }

        public SendMessage(string message)
        {
            Message = message;
        }
    }
}

﻿namespace ChatClient.Contract.Messages.Incoming
{
    public class Connect
    {
        public string UserName { get; private set; }

        public Connect(string userName)
        {
            UserName = userName;
        }
    }
}
